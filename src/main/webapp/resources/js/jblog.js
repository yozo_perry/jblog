function html(content){
	if(typeof(content) == "undefined")
		return "";
		
	content = content
		.replace(/'/g,  "&apos;")
		.replace(/"/g, "&quot;") // "
		.replace(/\t/g, "&nbsp;&nbsp;")// 替换跳格
		.replace(/ /g,  "&nbsp;")// 替换空格
		.replace(/</g,  "&lt;")
		.replace(/>/g,  "&gt;");

	return content;
}