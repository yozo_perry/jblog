package com.newflypig.jblog.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.newflypig.jblog.dao.ILianyihuiDAO;
import com.newflypig.jblog.model.Lianyihui;

@Repository("lianyihuiDao")
public class LianyihuiDAOImpl extends BaseHibernateDAO<Lianyihui> implements ILianyihuiDAO {

}
