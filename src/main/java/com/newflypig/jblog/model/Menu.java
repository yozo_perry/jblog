package com.newflypig.jblog.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToMany;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * JblogMenu entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "jblog_menu", catalog = "jblog2")
public class Menu implements java.io.Serializable {
	private static final long serialVersionUID = 3140725422939385736L;
	
	public static final String MENU_INDEX = "index";
	
	private Integer id;
	private String title;
	private String urlName;
	private Boolean isLeft;
	private String url;
	private Boolean show;
	private Set<Article> articles = new HashSet<Article>(0);

	public Menu() {
	}
	
	public Menu(Integer id){
		this.id = id;
	}

	public Menu(String title, Boolean isLeft, Boolean show) {
		this.title = title;
		this.isLeft = isLeft;
		this.show = show;
	}

	public Menu(String title, String urlName, Boolean isLeft, String url,
			Boolean show, Set<Article> articles) {
		this.title = title;
		this.urlName = urlName;
		this.isLeft = isLeft;
		this.url = url;
		this.show = show;
		this.articles = articles;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "title", nullable = false, length = 45)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "url_name", length = 45)
	public String getUrlName() {
		return this.urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	@Column(name = "is_left", nullable = false)
	public Boolean getIsLeft() {
		return this.isLeft;
	}

	public void setIsLeft(Boolean isLeft) {
		this.isLeft = isLeft;
	}

	@Column(name = "url", length = 80)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "show", nullable = false)
	public Boolean getShow() {
		return this.show;
	}

	public void setShow(Boolean show) {
		this.show = show;
	}

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "menus")
	public Set<Article> getArticles() {
		return this.articles;
	}

	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}

}