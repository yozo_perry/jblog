package com.newflypig.jblog.test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Test;

import com.newflypig.jblog.util.BlogUtils;
import com.newflypig.jblog.util.DESUtil;

public class BlogTests {
	//@Test
	public void testHtml(){
		System.out.println(BlogUtils.html("<script>alert('dd')</script>"));
	}
	
	//@Test
	public void testEncrypt() throws Exception{
		String key = "";
		StringBuffer keyBuffer = new StringBuffer();
		InputStream is = new FileInputStream("c:/jblog.token");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = reader.readLine();
		while(line != null){
			keyBuffer.append(line);
			line = reader.readLine();
		}
		reader.close();
		is.close();
		
		key = keyBuffer.toString();
		
		System.out.println(DESUtil.encryptString(key, "jdbc:mysql://121.42.170.67:3306/jblog2?useUnicode=true&characterEncoding=utf-8"));
	}
	
	@Test
	public void testDecrypt() throws Exception{
		String key = "";
		StringBuffer keyBuffer = new StringBuffer();
		InputStream is = new FileInputStream("c:/jblog.token");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = reader.readLine();
		while(line != null){
			keyBuffer.append(line);
			line = reader.readLine();
		}
		reader.close();
		is.close();
		
		key = keyBuffer.toString();
		System.out.println(DESUtil.decryptString(key, "oo+7uqEVC/W3FQPJTUdbEFCPjmsYoSbkk5n/uX8LyD7naZiiqBWnaIT1na85TOApl/xJczx9EyWri0GazU0pB3V9Vf0FSh1IIKbPJMfFw3w="));
	}
	
	//@Test
	public void testMath(){
		System.out.println(Math.ceil((double)19/10));
	}
}
